import axios from 'axios';
import { config } from '../base/config';

export const CategoryService = {
    all: async () => {
        try {
            const result = await axios
                .get(config.apiUrl + '/variants')
                .then((response) => {
                    //console.log(response);
                    const res = response.data
                    return {
                        success: res.success,
                        result: res.data,
                    };
                })
                .catch(error => {
                    return {
                        success: false,
                        result: error,
                    };
                });
            return result;
        } catch (error) {
            alert(error);
        }
    },
    getByid: async (id) => { 
        try {
            const result = await axios
                .get(config.apiUrl + '/variants/' + id)
                .then((response) => {
                    //console.log(response);
                    const res = response.data
                    return {
                        success: res.success,
                        result: res.data,
                    };
                })
                .catch(error => {
                    return {
                        success: false,
                        result: error,
                    };
                });
            return result;
        } catch (error) {
            alert(error);
        }
    },
    create: async () => { },
    update: async () => { },
    delete: async () => { },
};
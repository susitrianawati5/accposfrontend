import axios from 'axios';
import { ItemClick } from 'native-base/lib/typescript/components/composites/Typeahead/useTypeahead/types';
import { config } from '../base/config';

export const ProductService = {
    all: async () => {
        try {
            const result = await axios
                .get(config.apiUrl + '/products')
                .then((response) => {
                    //console.log(response);
                    const res = response.data
                    return {
                        success: res.success,
                        result: res.data,
                        pic: res.pic
                    };
                })
                .catch(error => {
                    return {
                        success: false,
                        result: error,
                    };
                });
            return result;
        } catch (error) {
            alert(error);
        }
    },
    getByid: async (id) => {
        try {
            const result = await axios
                .get(config.apiUrl + '/products/' + id)
                .then((response) => {
                    //console.log(response);
                    const res = response.data
                    return {
                        success: res.success,
                        result: res.data,
                    };
                })
                .catch(error => {
                    return {
                        success: false,
                        result: error,
                    };
                });
            return result;
        } catch (error) {
            alert(error);
        }
    },
    getProdByid: async (id) => {
        try {
            const result = await axios
                .get(config.apiUrl + '/productlist/' + id)
                .then((response) => {
                    //console.log(response);
                    const res = response.data
                    return {
                        success: res.success,
                        result: res.data,
                        pic: res.pic
                    };
                })
                .catch(error => {
                    return {
                        success: false,
                        result: error,
                    };
                });
            return result;
        } catch (error) {
            alert(error);
        }
    },
    create: async (item) => {
        item.activate = item.activate ? 1 : 0
        try {
            const result = await axios.post(config.apiUrl + '/products', item, {})
                .then(response => {
                    const res = response.data
                    // alert('Success : ' + response)
                    return {
                        success: res.success,
                        result: res.data
                    }
                })
                .catch(error => {
                    // alert('Error : ' + error)
                    console.log('error : ', error)
                    return {
                        success: false,
                        result: error
                    }
                })
            return result
        } catch (error) {
            alert('Error : ' + error)
        }
    },
    update: async (item) => { 
        item.activate = item.activate ? 1 : 0
        //console.log(item);
        try {
            const result = await axios.put(config.apiUrl + '/products/'+ item.id, item, {})
                .then(response => {
                    const res = response.data
                    // alert('Success : ' + response)
                    return {
                        success: res.success,
                        result: res.data
                    }
                })
                .catch(error => {
                    // alert('Error : ' + error)
                    console.log('error : ', error)
                    return {
                        success: false,
                        result: error
                    }
                })
            return result
        } catch (error) {
            alert('Error : ' + error)
        }
    },
    delete: async (id) => { 
        try {
            const result = await axios
                .delete(config.apiUrl + '/products/'+ id)
                .then((response) => {
                    const res = response.data
                    return {
                        success: res.success,
                        result: res.data,
                    };
                })
                .catch(error => {
                    return {
                        success: false,
                        result: error,
                    };
                });
            return result;
        } catch (error) {
            alert(error);
        }
    },
};
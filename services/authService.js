import {config} from '../base/config';
import axios from 'axios';

export const AuthService={
    login: async () => {
        try{
            const result = await axios.post(config.apiUrl + '/auth', userAuth, {})
            .then(response => {
                const res = response.data
                if (res.success){
                    return{
                        success: true,
                        result: {
                            user: res.user,
                            token: res.token
                        }
                       
                    }
                }else{
                    return{
                        success: false,
                        result: res.data
                    }
                }
            })
            .catch(error=>{
                alert('Error: '+ error)
                return{
                    success: false,
                    result: error
                }
            })
            return result;
        }catch(error){
            alert ('Error : '+ error)
        }
    }
}
import { StyleSheet } from "react-native";

const screenStyle = StyleSheet.create({
    container: {
        padding: 8
    },
    title: {
        fontSize: 18,
        color: 'brown',
        fontWeight: 'bold',
        marginBottom: 10
    },
    paragraph: {
        fontSize: 12
    },
    information: {
        fontSize: 14,
        fontStyle: 'italic'
    },
    quote: {
        fontWeight: 'bold'
    },
    successColor: {
        color: 'green'
    },
    view: {
        paddingBottom: 100
    },
    viewDetail: {
        flexDirection: 'row',
        backgroundColor: 'white',
        flex: 1,
        padding: 8,
        margin: 1.5,
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: {
            width: 0, height: 1
        },
        shadowOpacity: .8,
        shadowRadius: 2,
        elevation: 5
    },
    itemInnitial: {
        color: 'darkgoldenrod',
    
    },
    switchTrackColor: {
        false: 'gray',
        true: 'darkgoldenrod'
    },
    switchTransform: {
        transform: [{scaleX: .6},{scaleY: .6}]
    }
})

const modalStyle = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      // margin: 8
    },
    modalView: {
      margin: 20,
      width: '100%',
      backgroundColor: "white",
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "darkgoldenrod",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });

  const formStyle = StyleSheet.create({
    formTitle: {
      fontSize: 22,
      fontWeight: 'bold',
      color: 'darkgoldenrod',
      marginBottom: 4
    },
    label: {
      marginTop: 12,
      marginBottom: 4,
      fontSize: 20,
      color: 'darkgoldenrod'
    },
    input: {
      marginTop: 4,
      marginBottom: 4
    }
  })

export { screenStyle, modalStyle, formStyle }
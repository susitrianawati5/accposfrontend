/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import { Button, Text, StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from './components/home';
import About from './components/about';
import Categories from './components/categories';
import Variants from './components/variants';
import Orders from './components/orders';
import Product from './components/product';
// import Sales from './components/orders/sales';
// import ProSales from './components/orders/prosales';
import LatOrder from './components/latorder';
import Sales from './components/latorder/sales';

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarStyle: {
            height: 60,
            position: 'absolute',
            bottom: 16,
            right: 16,
            left: 16,
            borderRadius: 10,
            paddingTop: 4,
            paddingBottom: 4,
          }
        }}
      >
        <Tab.Screen
          name="Home"
          children={() => {
            return (
              <Home />
            )
          }}
          options={{
            tabBarActiveTintColor: 'darkgoldenrod',
            tabBarLabelStyle: { fontSize: 12 },
            tabBarLabel: 'Home',
            tabBarIcon: ({ color, size }) =>
              <MaterialCommunityIcons name="home" color={color} size={size} />
          }}
        />
        <Tab.Screen
          name="Categories"
          children={() => {
            return (
              <Categories />
            )
          }}
          options={{
            tabBarActiveTintColor: 'darkgoldenrod',
            tabBarLabelStyle: { fontSize: 12 },
            tabBarLabel: 'Categories',
            tabBarIcon: ({ color, size }) =>
              <MaterialCommunityIcons name="shape" color={color} size={size} />
          }}
        />
        <Tab.Screen
          name="Product"
          children={() => {
            return (
              <Product />
            )
          }}
          options={{
            tabBarActiveTintColor: 'darkgoldenrod',
            tabBarLabelStyle: { fontSize: 12 },
            tabBarLabel: 'Product',
            tabBarIcon: ({ color, size }) =>
              <MaterialCommunityIcons name="book-open" color={color} size={size} />
          }}
        />
        <Tab.Screen
          name="Orders"
          children={() => {
            return (
              <Orders />
            )
          }}
          options={{
            tabBarActiveTintColor: 'darkgoldenrod',
            tabBarLabelStyle: { fontSize: 12 },
            tabBarLabel: 'Orders',
            tabBarIcon: ({ color, size }) =>
              <MaterialCommunityIcons name="cart" color={color} size={size} />
          }}
        />
        <Tab.Screen
          name="LatihanOrder"
          children={() => {
            return (
              <LatOrder />
            )
          }}
          options={{
            tabBarActiveTintColor: 'darkgoldenrod',
            tabBarLabelStyle: { fontSize: 12 },
            tabBarLabel: 'Latihan Order',
            tabBarIcon: ({ color, size }) =>
              <MaterialCommunityIcons name="cart" color={color} size={size} />
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  )
}



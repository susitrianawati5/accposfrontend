import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Switch, Modal } from 'react-native';
import { Fab, Icon, Box, Center, Image, NativeBaseProvider } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { screenStyle, modalStyle } from '../../styles/commonStyle';

import ProductList from './productList'
import { CategoryService } from '../../services/categoryServices'
import { config } from '../../base/config';

import { useNavigation } from '@react-navigation/core';


export default class SalesComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            pic: ''
        }
    }

    add() {
        this.setState({
            modalVisible: true
        })
    }

    setModalVisible = (visible) => {
        //visible = true or false
        this.setState({
            modalVisible: visible
        })
    }
    render() {
        const { details, handleQuantity, addDetail, pic, submit } = this.props
        const picBase64 = 'data:image/jpeg;base64,' + pic;
        const urlPic = config.apiUrl + '/pic/';
        const executePayment=()=>{
            submit()
            this.props.navigation.goBack()
        }
        return (
            <NativeBaseProvider>
                <ScrollView style={screenStyle.container}>
                    <View style={screenStyle.view}>
                        {
                            details.map((item, idx) => {
                                return (
                                    <View key={'sls-' + idx} style={screenStyle.viewDetail}>
                                        <View style={{ flex: .18 }}>
                                            <Image key={(item.picName)} alt="No pic" style={{ width: "100%", height: "100%" }}
                                                source={{ uri: (item.picName ? urlPic + item.picName : picBase64) }} />
                                        </View>
                                        <View style={{ flex: .6 }}>
                                            <Text style={screenStyle.title}>  {item.productname}</Text>
                                            <Text style={screenStyle.itemInnitial}>  {item.price}</Text>
                                        </View>
                                        <View style={{ flex: .25 }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: .33 }}>
                                                    <MaterialCommunityIcons
                                                        name="minus-box-outline"
                                                        color="red"
                                                        size={24}
                                                        onPress={() => handleQuantity(idx, -1)}
                                                    />
                                                </View>
                                                <View style={{ flex: .33 }}>
                                                    <Text> {item.quantity} </Text>
                                                </View>
                                                <View style={{ flex: .33 }}>
                                                    <MaterialCommunityIcons
                                                        name="plus-box-outline"
                                                        color="navy"
                                                        size={24}
                                                        onPress={() => handleQuantity(idx, +1)}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                )
                            })
                        }
                        <View style={{ flex: .33 }}>
                            <Text style={screenStyle.title} onPress={() => executePayment()}> pay </Text>
                        </View>

                    </View>

                </ScrollView>

                <Box position="relative">
                    <Fab
                        bottom={85}
                        position="absolute"
                        size="sm"
                        colorScheme="pink"
                        onPress={() => {
                            this.add()
                        }}
                        icon={
                            <MaterialCommunityIcons
                                name="plus" color="white" size={20}
                            />
                        }
                    />
                </Box>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { this.setModalVisible(!this.state.modalVisible) }}
                >
                    <View style={[modalStyle.centeredView, { flex: 1, backgroundColor: 'rgba(0,0,0,.30)' }]}>
                        <View style={modalStyle.modalView}>
                            <ProductList setModalVisible={this.setModalVisible} addDetail={addDetail} submit={submit} />
                        </View>
                    </View>
                </Modal>
            </NativeBaseProvider>
        )
    }
}
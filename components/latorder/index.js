import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Switch, Modal, Pressable } from 'react-native';
import { Fab, Icon, Box, Center, NativeBaseProvider, Image } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

//style
import { screenStyle, modalStyle } from '../../styles/commonStyle';

//component
import { OrderService } from '../../services/orderService'
import SalesComponent from './sales';
import { useNavigation } from '@react-navigation/core';
//config
import { config } from '../../base/config'

export default class LatOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //membuat dummy untuk ordders
            orders: [],
            item: [],
            itemI: { date: '', reference: '', amount: 0 },
            details: [],
            amount: 0,
            pic: '',
            modalVisible: false
        }
    }

    componentDidMount() {
        this.load();
    }

    load = async () => {
        const response = await OrderService.all();
        if (response.success) {
            this.setState({
                orders: response.result,
                pic: response.pic
            })
        }
    }

    handleQuantity = (idx, value) => {
        const { details } = this.state;
        if (details[idx].quantity + value >= 0) {
            details[idx].quantity = details[idx].quantity + value
            const amount = details.map(i => (i.price * i.quantity)).reduce((prev, next) => prev + next, 0)
            this.setState({
                details: details,
                amount: amount
            })
        }
    }

    addDetail = (item) => {
        const { details } = this.state
        details.push({
            productid: item.id,
            productname: item.name,
            quantity: item.quantity,
            price: item.price,
            picname: item.picName,
            quantity: 1
        })
        const amount = details.map(i => (i.price * i.quantity)).reduce((prev, next) => prev + next, 0)
        this.setState({
            details: details,
            amount: amount
        })
        console.log(details)
        console.log(amount)
    }

    submit = async () => {
        const response = await OrderService.create({ amount: this.state.amount, details: this.state.details })
        if (response.success) {
            alert('Save success')
            this.setState({
                details: []
            })
            this.load()
        }
    }
    setModalVisible(visible) {
        //visible = true or false
        this.setState({
            modalVisible: visible
        })
    }
    getItem = async (id) => {
        const response = await OrderService.getByid(id)
        console.log(id)
        if (response.success) {
            if (response.result.length > 0) {
                // console.log(response.result)
                this.setState({
                    modalVisible: true,
                    item: response.result,
                    itemI: {
                        ...response.result[0],
                        activate: response.result[0].activate == "1"
                    },
                    pic: response.pic
                })
            } else {
                alert('Warning : \nItem not found')
            }
        } else {
            alert('Error load : \n' + response.result)
        }
    }
    render() {
        // this.state apabila masih satu file
        const { details, amount, modalVisible, itemI, item, pic } = this.state;
        const picBase64 = 'data:image/jpeg;base64,' + pic;
        const urlPic = config.apiUrl + '/pic/';
        const OrderScreen = ({ navigation }) => {
            const { orders } = this.state
            return (
                <NativeBaseProvider>
                    <ScrollView style={screenStyle.container}>
                        <View style={screenStyle.view}>
                            <Text style={[screenStyle.title]}>
                                ORDER PAGE
                            </Text>
                            {
                                orders.map((item, idx) => {
                                    return (

                                        <View key={'odr-' + idx} style={screenStyle.viewDetail}>
                                            <View style={{ flex: 0.3, color: 'brown' }}>
                                                <Text>id order</Text>
                                                <Text>Ref  </Text>
                                                <Text>Total  </Text>
                                                <Text>Date  </Text>
                                            </View>
                                            <View style={{ flex: 0.6 }}>
                                                <Text>: {item.id}</Text>
                                                <Text style={screenStyle.information}>: {item.reference}</Text>
                                                <Text>: {item.amount}</Text>
                                                <Text>: {item.date}</Text>
                                            </View>
                                            <View style={{ flex: 0.1 }}>
                                                <MaterialCommunityIcons
                                                    name="barcode"
                                                    onPress={() => this.getItem(item.id)}
                                                    color="sienna"
                                                    size={40}
                                                />
                                            </View>
                                        </View>
                                    )
                                })
                            }
                        </View>
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => { this.setModalVisible(!modalVisible) }}
                        >
                            <View style={[modalStyle.centeredView, { flex: 1, backgroundColor: 'rgba(0,0,0,.30)' }]}>
                                <View style={modalStyle.modalView}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 0.3 }}>
                                            <Text style={{ color: 'brown' }}>Tanggal </Text>
                                            <Text style={{ color: 'brown' }}>Ref  </Text>
                                            <Text style={{ color: 'brown' }}>Total  </Text>
                                        </View>
                                        <View style={{ flex: 0.6 }}>
                                            <Text style={{ color: 'brown' }}>: {itemI.date}</Text>
                                            <Text style={{ color: 'brown' }}>: {itemI.reference}</Text>
                                            <Text style={{ color: 'brown' }}>: {itemI.amount}</Text>
                                        </View>
                                    </View>

                                    {
                                        item.map((item, idx) => {
                                            return (
                                                <View key={'vw-' + idx} style={{ flexDirection: 'row' }}>
                                                    <View style={{ flex: .3, justifyContent: "center" }}>
                                                        <Image key={item.picName} alt="No pic" style={{ width: 70, height: 70 }}
                                                        source={{ uri: (item.picName ? urlPic + item.picName : picBase64) }} />
                                                    
                                                    </View>
                                                    <View style={{ flex: 0.3 }}>
                                                        <Text style={{ color: 'darkcyan' }}>Product  </Text>
                                                        <Text>Jumlah  </Text>
                                                        <Text>Harga  </Text>
                                                    </View>
                                                    <View style={{ flex: 0.6 }}>
                                                        <Text style={{ color: 'darkcyan' }}>: {item.nameProduct}</Text>
                                                        <Text>: {item.qty}</Text>
                                                        <Text>: Rp. {item.price}</Text>
                                                    </View>
                                                </View>
                                            )
                                        })
                                    }
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: .5, margin: 5 }}>
                                            <Pressable
                                                style={[modalStyle.button, modalStyle.buttonClose]}
                                                onPress={() => { this.setModalVisible(!modalVisible) }}
                                            >
                                                <Text style={{ textAlign: "center", color: "white" }}>Close</Text>
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                        <Box position="relative">
                            <Fab
                                bottom={85}
                                position="absolute"
                                size="sm"
                                colorScheme="purple"
                                onPress={() => {
                                    navigation.push('SalesScreen')
                                }}
                                icon={
                                    <MaterialCommunityIcons
                                        name="plus" color="white" size={20} />
                                }
                            />
                        </Box>
                    </ScrollView >
                </NativeBaseProvider >
            )
        }

        const SalesHeader = () => {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: .4 }}>
                        <Text style={{ fontSize: 24, color: "indigo" }}>Sales</Text>
                    </View>
                    <View style={{ flex: .6 }}>
                        <Text style={{ fontSize: 24, color: "indigo" }}>Total : {amount}</Text>
                    </View>
                </View>
            )
        }

        return (
            <Stack.Navigator>
                <Stack.Screen
                    name="OrderScreen"
                    options={{
                        headerShadow: false,
                        title: 'Orders'
                    }}
                >
                    {(props) => <OrderScreen {...props} />}
                </Stack.Screen>
                <Stack.Screen
                    name="SalesScreen"
                    options={{
                        title: "Sales",
                        headerTitle: () => <SalesHeader />
                    }}
                >
                    {(props) => <SalesComponent {...props} details={details} handleQuantity={this.handleQuantity} addDetail={this.addDetail} submit={this.submit} />}
                </Stack.Screen>
            </Stack.Navigator>

        )
    }
}
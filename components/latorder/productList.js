import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Switch, Pressable } from 'react-native';
import { Fab, Icon, Box, Center, Image, NativeBaseProvider, VStack, Select, CheckIcon } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { screenStyle, modalStyle } from '../../styles/commonStyle';
import { ProductService } from '../../services/productServices'
import { CategoryService } from '../../services/categoryServices';

import { config } from '../../base/config';

export default class ProductList extends Component {
    constructor() {
        super()
        this.state = {
            categories: [],
            list: [],
            item: { id: 0, categoryId: '', initName: '', name: '', description: '', price: 0, picName: '', activate: true },
            pic: ''
        }
    }
    componentDidMount() {
        this.load()
        this.loadCategories()
    }

    load = async () => {
        const response = await ProductService.all()
        if (response.success) {
            this.setState({
                list: response.result,
                pic: response.pic
            })
        } else {
            alert('Error: ' + response.result)
        }
    }
    loadCategories = async () => {
        const response = await CategoryService.all()
        if (response.success) {
            this.setState({
                categories: response.result
            })
        }
    }

    getItem = async (id) => {
        const response = await ProductService.getProdByid(id)
        console.log(id)
        console.log(response.result)
        if (response.success) {
            if (response.result.length > 0) {
                this.setState({
                    modalVisible: true,
                    list: response.result,
                    pic: response.pic
                })
            } else {
                alert('Warning : \nItem not found')
            }
        } else {
            alert('Error load : \n' + response.result)
        }
    }
    render() {
        const { setModalVisible, addDetail } = this.props
        const { list, pic, categories } = this.state
        const picBase64 = 'data:image/jpeg;base64,' + pic;
        const urlPic = config.apiUrl + '/pic/';
        return (
            <View style={{ width: "100%", height: "100%" }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: .9 }}>
                        <Text style={screenStyle.title}>Select Produt list</Text>
                    </View>
                    <View style={{ flex: .1 }}>
                        <MaterialCommunityIcons
                            name="close-circle"
                            color="indigo"
                            size={32}
                            onPress={setModalVisible}
                        />
                    </View>
                </View>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    {
                        categories.map((item, idx) => {
                            return (
                                <View key={'cat-' + idx} style={[screenStyle.viewDetail, { width: 100, height: 80, backgroundColor: 'lightcyan' }]}>
                                    <Pressable onPress={() => { this.getItem(item.id) }}>
                                        <Text style={[{ textAlignVertical: 'center', textAlign: 'center' }]}>{item.name}</Text>
                                    </Pressable>
                                </View>
                            )
                        })
                    }
                </ScrollView>
                <ScrollView>

                    {list.map((item, idx) => {
                        return (
                            <View key={'prod-' + item.id}>
                                <View
                                    style={screenStyle.viewDetail}
                                    onTouchEnd={() => {
                                        addDetail(item);
                                        setModalVisible(false)
                                    }}
                                >

                                    <View style={{ flex: .3, justifyContent: "center" }}>
                                        <Image key={item.picName} alt="No pic" style={{ width: 70, height: 70 }}
                                            source={{ uri: (item.picName ? urlPic + item.picName : picBase64) }} />
                                        <Text style={{ color: 'brown' }}>Rp. {item.price}</Text>
                                    </View>
                                    <View style={{ flex: .7, padding: 5 }}>
                                        <Text style={screenStyle.title}>{item.name}</Text>
                                        <Text style={screenStyle.itemInnitial}>{item.description}</Text>
                                        <Text style={{ color: 'gray' }}>From : {item.initCat}</Text>
                                    </View>
                                </View>
                            </View>
                        );
                    })
                    }
                </ScrollView>
            </View>
        )
    }
}
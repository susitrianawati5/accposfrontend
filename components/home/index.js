import React, { Component } from 'react'
import { View, Text, ScrollView, StyleSheet, Image } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { screenStyle } from '../../styles/commonStyle'

export default class Home extends Component {
    render(){
        return(
            <ScrollView style={screenStyle.container}>
                <Text style={screenStyle.title}>
                    ACC POS
                </Text>
                <Text style={[screenStyle.paragraph, screenStyle.successColor]}>
                    This is from home
                </Text>
                <Text style={[screenStyle.information, screenStyle.successColor]}>
                    This app will soft launch tomorrow
                </Text>
                <Text style={[screenStyle.quote, screenStyle.information]}>
                    "Growing up together"
                </Text>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, backgroundColor: 'yellow' }}>
                        <Text style={screenStyle.title}>
                            This is from Home
                        </Text>
                    </View>
                    <View style={{ flex: 1, backgroundColor: 'pink' }}>
                        <View style={{ width: 50, height: 50, backgroundColor: 'brown' }} >
                            <MaterialCommunityIcons
                                name="home" color="white" size={20}/>
                        </View>
                        <View style={{ flex: 0.5 }}>
                            <Text style={screenStyle.paragraph}>
                                this home is description from this Appuinhveomkpa,vmhv kjh iebu hisenyhuiey hfyehjs yih,dkugil ,esbyt h,ezkbty ghdjjt gjdybghjd
                            </Text>
                        </View>

                    </View>
                </View>
                <View style={{ flex: 0.2 }}>
                    <Image source={{ uri: 'https://reactjs.org/logo-og.png' }}
                        style={{ width: 40, height: 40 }} />
                </View>
                <Text style={screenStyle.title}>
                    This is from Home
                </Text>
                <Text style={screenStyle.paragraph}>
                    this home is description from this App
                </Text>
            </ScrollView>
        )
    }
} 

// const styles = StyleSheet.create({
//     container: {
//       padding: 8
//     },
//     title: {
//       fontSize: 18,
//       color: 'black'
//     },
//     paragraph: {
//       fontSize: 14
//     }
//   })
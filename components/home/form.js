
import React, { Component } from 'react';
import { Input, FormControl, VStack, Select, CheckIcon,  Center, ScrollView } from 'native-base';
import { View, Text, Switch, TextInput } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { screenStyle, modalStyle, formStyle } from '../../styles/commonStyle';

import { OrderService } from '../../services/orderService'
import { config } from '../../base/config';

export default class sss extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { item, handleChange, handleChangeCheckBox, crudAction, categories } = this.props
        return (
            <ScrollView style={screenStyle.container}>
                <FormControl
                w={{ base: "100%" }}
            >
                <View>
                    <Text style={formStyle.formTitle}>
                        {crudAction == 0 ? 'Create' : crudAction == 1 ? 'Update' : 'Delete'} Product
                    </Text>
                    <Text>
                        {crudAction == 2 ? 'Are you sure' : ''}
                    </Text>
                    <Text>{JSON.stringify(item)}</Text>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Tanggal :</FormControl.Label>
                    <Input value={item.date} onChangeText={value => handleChange('date',value)} isReadOnly={crudAction==2}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Ref : </FormControl.Label>
                    <Input value={item.reference} onChangeText={value => handleChange('reference',value)} isReadOnly={crudAction==2}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Ref : </FormControl.Label>
                    <Input keyboardType="numeric" value={item.amount} onChangeText={value => handleChange('amount',value)} isReadOnly={crudAction==2}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Id Product:</FormControl.Label>
                    <Input keyboardType="numeric" value={item.idProduct} onChangeText={value => handleChange('idProduct',value)} isReadOnly={crudAction==2}/>
                </View>
            </FormControl>
            </ScrollView>
        )
    }
}
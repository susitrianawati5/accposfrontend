import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {Button, NativeBaseProvider, FromControl, Input } from 'react-native-base'
import AsyncStorage  from '@react-native-community/async-storage'

import {AuthService} from '../../services/authService'

import { screenStyle, formStyle} from '../../styles/commonStyle'

export default class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            user: { userName: '', password:''}
        }
    }
    loginAuth= async()=>{
        const {user} = this.state;
        const response = await AuthService.login(user)
        if(response.success){
            AsyncStorage.setItem('user_info', response.result.user)
            AsyncStorage.setItem('user_token', response.result.token)
        }
    }
}
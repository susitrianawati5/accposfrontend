import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Switch, Modal, Pressable } from 'react-native';
import { Fab, Icon, Box, Center, NativeBaseProvider, Image } from 'native-base'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Popover from 'react-native-popover-view'

import { VariantService } from '../../services/variantService'

import { screenStyle, modalStyle } from '../../styles/commonStyle';

import { OrderService } from '../../services/orderService'
import { useNavigation } from '@react-navigation/core';
//config
import { config } from '../../base/config'

function Sales() {
    const navigation = useNavigation()

    return (
        <Box position="relative">
            <Fab
                bottom={85}
                position="absolute"
                onPress={() => navigation.navigate('Sales')}
                size="sm"
                colorScheme="cyan"
                icon={
                    <MaterialCommunityIcons
                        name="plus"
                        color="white"
                        size={20}
                    />
                }
            />
        </Box>
    )

}

export default class Order extends Component {
    constructor() {
        super();
        this.state = {
            list: [],
            item: [],
            itemI: { date: '', reference: '', amount: 0 },
            pic: '',
            modalVisible: false
        }
    }

    componentDidMount() {
        this.load()
    }

    load = async () => {
        const response = await OrderService.all()
        if (response.success) {
            console.log(response.result)
            this.setState({
                list: response.result
            })
        } else {
            alert('Error: ' + response.result)
        }
    }
    setModalVisible(visible) {
        //visible = true or false
        this.setState({
            modalVisible: visible
        })
    }
    getItem = async (id) => {
        const response = await OrderService.getByid(id)
        console.log(id)
        if (response.success) {
            if (response.result.length > 0) {
                // console.log(response.result)
                this.setState({
                    modalVisible: true,
                    item: response.result,
                    itemI: {
                        ...response.result[0],
                        activate: response.result[0].activate == "1"
                    },
                    pic: response.pic
                })
            } else {
                alert('Warning : \nItem not found')
            }
        } else {
            alert('Error load : \n' + response.result)
        }
    }

    render() {
        const { list, modalVisible, item, itemI, pic } = this.state
        const picBase64 = 'data:image/jpeg;base64,' + pic
        const urlPic = config.apiUrl + '/pic/'
        return (
            <NativeBaseProvider>
                <ScrollView style={screenStyle.container}>
                    <View style={screenStyle.view}>
                        <Text style={[screenStyle.title]}>
                            ORDER PAGE
                        </Text>
                        {
                            list.map((item, idx) => {
                                return (
                                    <View key={idx} style={screenStyle.viewDetail}>
                                        <View style={{ flex: 0.3, color: 'brown' }}>
                                            <Text>Tanggal order</Text>
                                            <Text>Ref  </Text>
                                            <Text>Total  </Text>
                                        </View>
                                        <View style={{ flex: 0.6 }}>
                                            <Text>: {item.date}</Text>
                                            <Text style={screenStyle.information}>: {item.reference}</Text>
                                            <Text>: {item.amount}</Text>
                                        </View>
                                        <View style={{ flex: 0.1 }}>
                                            <MaterialCommunityIcons
                                                name="barcode"
                                                onPress={() => this.getItem(item.id)}
                                                color="sienna"
                                                size={40}
                                            />
                                        </View>
                                    </View>
                                )
                            })
                        }
                    </View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => { this.setModalVisible(!modalVisible) }}
                    >
                        <View style={[modalStyle.centeredView, { flex: 1, backgroundColor: 'rgba(0,0,0,.30)' }]}>
                            <View style={modalStyle.modalView}>
                                <View style={{ flexDirection: 'row'}}>
                                    <View style={{ flex: 0.3 }}>
                                        <Text style={{color:'brown'}}>Tanggal </Text>
                                        <Text style={{color:'brown'}}>Ref  </Text>
                                        <Text style={{color:'brown'}}>Total  </Text>
                                    </View>
                                    <View style={{ flex: 0.6 }}>
                                        <Text style={{color:'brown'}}>: {itemI.date}</Text>
                                        <Text style={{color:'brown'}}>: {itemI.reference}</Text>
                                        <Text style={{color:'brown'}}>: {itemI.amount}</Text>
                                    </View>
                                </View>

                                {
                                    item.map((item, idx) => {
                                        return (
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: .3, justifyContent: "center" }}>
                                                    <Image key={item.picname} alt="No pic" style={{ width: 70, height: 70 }}
                                                        source={{ uri: (item.picname ? urlPic + item.picname : picBase64) }} />
                                                    
                                                </View>
                                                <View style={{ flex: 0.3 }}>
                                                    <Text style={{color: 'darkcyan'}}>Product  </Text>
                                                    <Text>Jumlah  </Text>
                                                    <Text>Harga  </Text>
                                                </View>
                                                <View style={{ flex: 0.6 }}>
                                                    <Text style={{color: 'darkcyan'}}>: {item.nameProduct}</Text>
                                                    <Text>: {item.qty}</Text>
                                                    <Text>: Rp. {item.price}</Text>
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: .5, margin: 5 }}>
                                        <Pressable
                                            style={[modalStyle.button, modalStyle.buttonClose]}
                                            onPress={() => { this.setModalVisible(!modalVisible) }}
                                        >
                                            <Text style={{ textAlign: "center", color: "white" }}>Close</Text>
                                        </Pressable>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    <Sales />
                </ScrollView >
            </NativeBaseProvider >
        )
    }
}
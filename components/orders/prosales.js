import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Switch, Pressable } from 'react-native';
import { Fab, CheckIcon, Box, Center, Image, NativeBaseProvider, VStack, FormControl, Select } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { screenStyle, modalStyle, formStyle } from '../../styles/commonStyle';

import { ProductService } from '../../services/productServices'
import { CategoryService } from '../../services/categoryServices';

import { config } from '../../base/config';

export default class ProSales extends Component {
    constructor() {
        super();
        this.state = {
            categories: [],
            list: [],
            item: { id: 0, categoryId: '', initName: '', name: '', description: '', price: 0, picName: '', activate: true },
            pic: ''
        }
    }

    componentDidMount() {
        this.load()
        this.loadCategories()
    }

    load = async () => {
        const response = await ProductService.all()
        if (response.success) {
            this.setState({
                list: response.result,
                pic: response.pic
            })
        } else {
            alert('Error: ' + response.result)
        }
    }
    loadCategories = async () => {
        const response = await CategoryService.all()
        if (response.success) {
            this.setState({
                categories: response.result
            })
        }
    }

    getItem = async (id) => {
        const response = await ProductService.getProdByid(id)
        console.log(id)
        console.log(response.result)
        if (response.success) {
            if (response.result.length > 0) {
                this.setState({
                    modalVisible: true,
                    list: response.result,
                    pic: response.pic
                })
            } else {
                alert('Warning : \nItem not found')
            }
        } else {
            alert('Error load : \n' + response.result)
        }
    }
    render() {
        const { list, pic, categories} = this.state
        const picBase64 = 'data:image/jpeg;base64,' + pic;
        const urlPic = config.apiUrl + '/pic/';
        return (
            <NativeBaseProvider>
                <ScrollView style={screenStyle.container}>
                    <View style={screenStyle.view}>
                        <Text style={[screenStyle.title]}>
                            Produt in sales
                        </Text>
                        <View>
                            <VStack alignItems="center" space={4}>
                                <Select
                                    minWidth="200"
                                    accessibilityLabel="Coose Category"
                                    placeholder="Choose Category"
                                    _selectedItem={{
                                        bg: "#00ADB5",
                                        endIcon: <CheckIcon size="5" />
                                    }}
                                    mt={1}
                                    onValueChange={(value) => { value == "0" ? this.load() : this.getItem(value) }}
                                >
                                    <Select.Item label="No Category" value="0" />
                                    {
                                        categories.map((cat, idx) => {
                                            return <Select.Item key={cat.id} label={cat.name} value={cat.id} />
                                        })
                                    }
                                </Select>
                            </VStack>
                        </View>
                        {list.map((item, idx) => {
                            return (
                                <Pressable onPress={() =>{this.props.navigation.navigate('Sales')}}>
                                <View key={idx} style={[screenStyle.viewDetail, { opacity: (Boolean(item.activate) ? 1 : .5) }]}>
                                    <View style={{ flex: .3, justifyContent: "center" }}>
                                        <Image key={item.picName} alt="No pic" style={{ width: 70, height: 70 }}
                                            source={{ uri: (item.picName ? urlPic + item.picName : picBase64) }} />
                                        <Text style={{ color: 'brown' }}>Rp. {item.price}</Text>
                                    </View>
                                    <View style={{ flex: .7, padding: 5 }}>
                                        <Text style={screenStyle.title}>{item.name}</Text>
                                        <Text style={screenStyle.itemInnitial}>{item.description}</Text>
                                        <Text style={{ color: 'gray' }}>From : {item.initCat}</Text>
                                    </View>
                                    <View style={{ flex: .2 }}>
                                        <Switch
                                            trackColor={screenStyle.switchTrackColor}
                                            thumbColor="darkcyan"
                                            style={screenStyle.switchTransform}
                                            value={item.activate == 1}
                                        />
                                    </View>
                                </View>
                                </Pressable>
                            );
                        })
                        }
                    </View>
                </ScrollView>
            </NativeBaseProvider>
        )
    }
}
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Switch } from 'react-native';
import { Fab, Icon, Box, Center, Image, NativeBaseProvider} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { screenStyle, modalStyle } from '../../styles/commonStyle';

import { OrderService } from '../../services/orderService'
import { config } from '../../base/config';

import { useNavigation } from '@react-navigation/core';

function ProductMove() {
    const navigation = useNavigation()
    return (
        <Box position="relative">
            <Fab
                bottom={85}
                position="absolute"
                size="sm"
                colorScheme="pink"
                onPress={() => navigation.navigate('ProSales')}
                icon={
                    <MaterialCommunityIcons
                        name="plus" color="white" size={20}
                    />
                }
            />
        </Box>
    )
}
export default class Sales extends Component {
    constructor() {
        super();
        this.state = {
            list: [],
            item: {
                amount: 0, price: 0
            },
            pic: ''
        }
    }

    componentDidMount() {
        this.load()
    }

    load = async () => {
        const response = await OrderService.all()
        if (response.success) {
            this.setState({
                list: response.result
            })
        } else {
            alert('Error: ' + response.result)
        }
    }
    render() {
        const { list, item, pic } = this.state
        const picBase64 = 'data:image/png;base64,' + pic;
        const urlPic = config.apiUrl + '/pic/';
        return (
            <NativeBaseProvider>
                <ScrollView style={screenStyle.container}>
                    <View style={screenStyle.view}>
                        <Text style={[screenStyle.title]}>
                            SALES PAGE
                        </Text>
                        <View style={screenStyle.viewDetail}>
                            <View>
                                <Text>Total: {item.amount}</Text>
                            </View>
                        </View>
                        {
                            list.map((item, idx) => {
                                return (
                                    <View key={idx} style={screenStyle.viewDetail}>
                                        {/* <View style={{ flex: .25, paddingRight: 8 }}>
                                        <Image key={(item.picname)} alt="No pic" style={{ width: "100%", height: "100%" }}
                                            source={{ uri: (item.picname ? urlPic + item.picname : picBase64) }} />
                                    </View> */}
                                        <View style={{ flex: 0.3, color: 'pink' }}>
                                            <Text>{item.nameProduct}</Text>
                                            <Text>{item.price}</Text>
                                        </View>
                                        <View style={{ flex: 0.3, color: 'pink' }}>
                                            <Text>{item.nameProduct}</Text>
                                            <Text>{item.price}</Text>
                                        </View>
                                    </View>
                                )
                            })
                        }
                    </View>
                    <ProductMove/>
                </ScrollView>
            </NativeBaseProvider>
        )
    }
}
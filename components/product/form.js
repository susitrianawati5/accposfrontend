import React, { Component } from 'react';
import { Input, FormControl, VStack, Select, CheckIcon,  Center, ScrollView } from 'native-base';
import { View, Text, Switch, TextInput } from 'react-native';

//style
import { formStyle } from '../../styles/commonStyle'

export default class FormProduct extends Component {
    constructor(props) {
        super(props)
    }
    render(){
        const { item, handleChange, handleChangeCheckBox, crudAction, categories } = this.props
        return(
            <ScrollView>
            <FormControl
                w={{ base: "100%" }}
            >
                <View>
                    <Text style={formStyle.formTitle}>
                        {crudAction == 0 ? 'Create' : crudAction == 1 ? 'Update' : 'Delete'} Product
                    </Text>
                    <Text>
                        {crudAction == 2 ? 'Are you sure' : ''}
                    </Text>
                    <Text>{JSON.stringify(item)}</Text>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Category:</FormControl.Label>
                    <VStack alignItems="center" space={4}>
                        <Select
                            selectedValue={item.categoryId}
                            minWidth="200"
                            isDisabled={crudAction==2}
                            accessibilityLabel="Coose Category"
                            placeholder="Choose Category"
                            _selectedItem={{
                                bg: "teal.600",
                                endIcon:<CheckIcon size="5"/>
                            }}
                            mt={1}
                            onValueChange={value => handleChange('categoryId', value)}
                        >
                            <Select.Item label="No Category" value="0"/>
                            {
                                categories.map((cat, idx) => {
                                    return <Select.Item key={cat.id} label={cat.name} value={cat.id}/>
                                })
                            }
                        </Select>
                    </VStack>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Initial:</FormControl.Label>
                    <Input value={item.initName} onChangeText={value => handleChange('initName',value)} isReadOnly={crudAction==2}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Name:</FormControl.Label>
                    <Input value={item.name} onChangeText={value => handleChange('name',value)} isReadOnly={crudAction==2}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Price:</FormControl.Label>
                    <Input keyboardType="numeric" value={item.price.toString()} onChangeText={value => handleChange('price',value)} isReadOnly={crudAction==2}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Description:</FormControl.Label>
                    <Input value={item.description} onChangeText={value => handleChange('description',value)} isReadOnly={crudAction==2}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Picture:</FormControl.Label>
                    <Input value={item.picName} onChangeText={value => handleChange('picName',value)} isReadOnly={crudAction==2}/>
                </View>
                <View style={{ flexDirection: 'row', marginTop:10, marginBottom:10 }}>
                    <View style={{ flex: .1, marginLeft:12 }}>
                        <Switch
                            trackColor={{ false: 'gray', true: '#00ADB5' }}
                            thumbColor="#f4f3f4"
                            isDisabled={crudAction==2}
                            disabled={crudAction==2}
                            value={item.activate}
                            onValueChange={() => {handleChangeCheckBox('activate')}}
                        />
                    </View>
                    <View>
                        <Text>Activate</Text>
                    </View>
                </View>
            </FormControl>
            </ScrollView>
        )
    }
}
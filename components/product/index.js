import React, { Component } from 'react';

//UI components
import { View, Text, StyleSheet, ScrollView, Switch, Modal, Pressable } from 'react-native';
import { Fab, Icon, Box, Center, NativeBaseProvider, Image } from 'native-base'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Popover from 'react-native-popover-view'

//style
import { screenStyle, modalStyle } from '../../styles/commonStyle';

//component
import FormProduct from './form'

//services
import { ProductService } from '../../services/productServices'
import { CategoryService } from '../../services/categoryServices';

//config
import { config } from '../../base/config'

export default class ProductComponent extends Component {
    constructor() {
        super()
        this.state = {
            categories: [],
            list: [],
            item: { id: 0, categoryId: '', initName: '', name: '', description: '', price: 0, picName: '', activate: true },
            pic: '',
            crudAction: 0,
            modalVisible: false,
        }
    }

    componentDidMount() {
        this.load();
        this.loadCategories()
    }

    load = async () => {
        const response = await ProductService.all()
        if (response.success) {
            this.setState({
                list: response.result,
                pic: response.pic
            })
        }
    }

    loadCategories = async () => {
        const response = await CategoryService.all()
        if (response.success) {
            this.setState({
                categories: response.result
            })
        }
    }

    handleChange = (name, value) => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: value
            }
        })
    }

    handleChangeCheckBox = name => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: !this.state.item.activate
            }
        })
    }

    setModalVisible(visible) {
        //visible = true or false
        this.setState({
            modalVisible: visible
        })
    }

    create() {
        this.loadCategories()
        this.setState({
            modalVisible: true,
            item: { id: 0, categoryId: '', initName: '', name: '', description: '', price: 0, activate: true, picName: '' },
            crudAction: 0
        })
    }

    async submit() {
        const { item, crudAction } = this.state
        if (crudAction == 0) {
            //create new
            const res = await ProductService.create(item)
            console.log(res)
            if (res.success) {
                alert('Create Success: \n' + res.result.name + '\nhas been save')
                this.setState({
                    modalVisible: false
                })
                await this.load()
            } else {
                alert('Error : \n' + res.result)
            }
        } else if (crudAction == 1) {
            //update
            const res = await ProductService.update(item)
            console.log(res);
            if (res.success) {
                alert('Update Success: \n' + res.result.name + '\nhas been save')
                this.setState({
                    modalVisible: false
                })
                await this.load()
            } else {
                alert('Error : \n' + res.result)
            }
        } else {
            const res = await ProductService.delete(item.id)
            if (res.success) {
                alert('Delete Success')
                this.setState({
                    modalVisible: false
                })
                await this.load()
            } else {
                alert('Error : \n' + res.result)
            }
        }
    }

    getItem = async (id, action) => {
        const response = await ProductService.getByid(id)
        console.log(id)
        if (response.success) {
            if (response.result.length > 0) {
                //console.log(response.result[0].activate ? '')
                this.setState({
                    modalVisible: true,
                    item: {
                        ...response.result[0],
                        activate: response.result[0].activate == "1"
                    },
                    crudAction: action
                })
            } else {
                alert('Warning : \nItem not found')
            }
        } else {
            alert('Error load : \n' + response.result)
        }
    }

    render() {
        const { categories, list, item, pic, modalVisible, crudAction } = this.state
        const picBase64 = 'data:image/jpeg;base64,' + pic
        const urlPic = config.apiUrl + '/pic/'

        return (
            <NativeBaseProvider>
                <ScrollView style={screenStyle.container}>
                    <View style={screenStyle.view}>
                        <Text style={screenStyle.title}>Welcome to Product</Text>
                        {list.map((item, idx) => {
                            return (
                                <View key={idx} style={[screenStyle.viewDetail, { opacity: (Boolean(item.activate) ? 1 : .5) }]}>
                                    <View style={{ flex: .3, justifyContent: "center" }}>
                                        <Image key={item.picName} alt="No pic" style={{ width: 70, height: 70 }}
                                            source={{ uri: (item.picName ? urlPic + item.picName : picBase64) }} />
                                            <Text style={{ color: 'brown' }}>Rp. {item.price}</Text>
                                    </View>
                                    <View style={{ flex: .7, padding: 5 }}>
                                        <Text style={screenStyle.title}>{item.name}</Text>
                                        <Text style={screenStyle.itemInnitial}>{item.description}</Text>
                                        <Text style={{ color: 'gray' }}>From : {item.initCat}</Text>
                                    </View>
                                    <View style={{ flex: .2 }}>
                                        <Switch
                                            trackColor={screenStyle.switchTrackColor}
                                            thumbColor="darkcyan"
                                            style={screenStyle.switchTransform}
                                            value={item.activate==1}
                                        />
                                    </View>
                                    <Popover
                                        from={(
                                            <Pressable>
                                                <MaterialCommunityIcons
                                                    name="menu"
                                                    color="darkgoldenrod"
                                                    size={30}
                                                />
                                            </Pressable>
                                        )}>
                                        <View style={{ flex: 1}}>
                                            
                                            <Text 
                                                style={{ padding:8, margin:4, backgroundColor: 'teal', color: "white" }}
                                                onPress={() => this.getItem(item.id, 1)}>Edit</Text>
                                        </View>
                                        <View style={{ flex: 0.1 }}>
                                            
                                            <Text 
                                                style={{ padding:8, margin:4, backgroundColor: 'brown', color: "white" }} 
                                                onPress={() => this.getItem(item.id, 2)}
                                            >Delete</Text>
                                        </View>
                                    </Popover>
                                </View>
                            );
                        })
                        }
                    </View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => { this.setModalVisible(!modalVisible) }}
                    >
                        <View style={[modalStyle.centeredView, { flex: 1, backgroundColor: 'rgba(0,0,0,.30)' }]}>
                            <View style={modalStyle.modalView}>
                                <FormProduct
                                    categories={categories}
                                    item={item}
                                    handleChange={this.handleChange}
                                    handleChangeCheckBox={this.handleChangeCheckBox}
                                    crudAction={crudAction}
                                />
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: .5, margin: 5 }}>
                                        <Pressable
                                            style={[modalStyle.button, modalStyle.buttonClose]}
                                            onPress={() => { this.submit() }}
                                        >
                                            <Text style={{ textAlign: "center", color: "white" }}>Save</Text>
                                        </Pressable>
                                    </View>
                                    <View style={{ flex: .5, margin: 5 }}>
                                        <Pressable
                                            style={[modalStyle.button, modalStyle.buttonClose]}
                                            onPress={() => { this.setModalVisible(!modalVisible) }}
                                        >
                                            <Text style={{ textAlign: "center", color: "white" }}>Close</Text>
                                        </Pressable>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    <Box position="relative">
                        <Fab
                            bottom={85}
                            position="absolute"
                            size="sm"
                            colorScheme="cyan"
                            onPress={() => { this.create() }}
                            icon={
                                <MaterialCommunityIcons
                                    name="plus"
                                    color="white"
                                    size={20}
                                />
                            }
                        />
                    </Box>
                </ScrollView>
            </NativeBaseProvider>
        )
    }
}
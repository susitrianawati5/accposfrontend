import React, { Component } from 'react';
import { Input, FormControl } from 'native-base';
import { View, Text, Switch } from 'react-native';

//style
import { formStyle } from '../../styles/commonStyle'

export default class FormCategory extends Component {
    constructor(props) {
        super(props)
    }
    render(){
        const { item, handleChange, handleChangeCheckBox, crudAction } = this.props
        return(
            <FormControl
                w={{ base: "100%" }}
            >
                <View>
                    <Text style={formStyle.formTitle}>
                        {crudAction == 0 ? 'Create' : crudAction == 1 ? 'Update' : 'Delete'} Category
                    </Text>
                    <Text>
                        {crudAction == 2 ? 'Are you sure' : ''}
                    </Text>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Initial:</FormControl.Label>
                    <Input disabled={crudAction==2} value={item.initName} onChangeText={value => handleChange('initName',value)}/>
                </View>
                <View>
                    <FormControl.Label style={formStyle.label}>Name:</FormControl.Label>
                    <Input disabled={crudAction==2} value={item.name} onChangeText={value => handleChange('name',value)}/>
                </View>
                <View style={{ flexDirection: 'row', marginTop:10, marginBottom:10 }}>
                    <View style={{ flex: .1, marginLeft:12 }}>
                        <Switch
                            trackColor={{ false: 'gray', true: '#00ADB5' }}
                            thumbColor="#f4f3f4"
                            disabled={crudAction==2}
                            value={item.activate}
                            onValueChange={() => {handleChangeCheckBox('activate')}}
                        />
                    </View>
                    <View>
                        <Text>Activate</Text>
                    </View>
                </View>
            </FormControl>
        )
    }
}
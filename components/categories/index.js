import React, { Component } from 'react';

//UI component
import { View, Text, StyleSheet, ScrollView, Switch, Modal, Pressable } from 'react-native';
import { Fab, Icon, Box, Center, NativeBaseProvider } from 'native-base'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Popover from 'react-native-popover-view'

//style
import { screenStyle, modalStyle } from '../../styles/commonStyle';

//services
import { CategoryService } from '../../services/categoryServices';

//Components
import FormCategory from './form'

export default class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {//penampung data sementara yg terikat dg komponen
            list: [],
            item: { id: 0, initName: 'buff', name: 'yakisoba', activate: true },
            modalVisible: false,
            crudAction: 0
        }
        //crudAction: 0 = create, 1 = edit, 2 = delete
    }

    componentDidMount() {
        this.load();
    }

    load = async () => {
        const response = await CategoryService.all();
        if (response.success) {
            this.setState({
                list: response.result,
            });
        } else {
            alert('Error: ' + response.result);
        }
    };

    setModalVisible(visible) {
        //visible = true or false
        this.setState({
            modalVisible: visible
        })
    }

    handleChange = (name, value) => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: value
            }
        })
    }

    handleChangeCheckBox = name => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: !this.state.item.activate
            }
        })
    }

    create() {
        this.setState({
            modalVisible: true,
            item: { id: 0, initName: '', name: '', activate: true },
            crudAction: 0
        })
    }

    getItem = async (id, action) => {
        const response = await CategoryService.getByid(id)
        console.log(id)
        if (response.success) {
            if (response.result.length > 0) {
                //console.log(response.result[0].activate ? '')
                this.setState({
                    modalVisible: true,
                    item: {
                        ...response.result[0],
                        activate: response.result[0].activate == "1"
                    },
                    crudAction: action
                })
            } else {
                alert('Warning : \nItem not found')
            }
        } else {
            alert('Error load : \n' + response.result)
        }
    }

    async submit() {
        const { item, crudAction } = this.state
        if (crudAction == 0) {
            //create new
            const res = await CategoryService.create(item)
            if (res.success) {
                alert('Create Success: \n' + res.result.name + '\nhas been save')
                this.setState({
                    modalVisible: false
                })
                await this.load()
            } else {
                alert('Error : \n' + res.result)
            }
        } else if (crudAction == 1) {
            //update
            const res = await CategoryService.update(item)
            console.log(res);
            if (res.success) {
                alert('Update Success: \n' + res.result.name + '\nhas been save')
                this.setState({
                    modalVisible: false
                })
                await this.load()
            } else {
                alert('Error : \n' + res.result)
            }
        } else {
            const res = await CategoryService.delete(item.id)
            if (res.success) {
                alert('Delete Success')
                this.setState({
                    modalVisible: false
                })
                await this.load()
            } else {
                alert('Error : \n' + res.result)
            }
        }
    }

    render() {
        const { list, item, modalVisible, crudAction } = this.state;
        return (
            <NativeBaseProvider>
                <ScrollView style={screenStyle.container}>
                    <View style={screenStyle.view}>
                        <Text style={screenStyle.title}>Welcome to Category</Text>
                        {/* <Text>{JSON.stringify(item)}</Text> */}
                        {list.map((item, idx) => {
                            return (
                                <View key={idx} style={screenStyle.viewDetail}>
                                    <View style={{ flex: .13 }}>
                                    <MaterialCommunityIcons
                                                name="food"
                                                color="sienna"
                                                size={40}
                                            />
                                    </View>
                                    <View style={{ flex: 0.7 }}>
                                        <Text style={screenStyle.itemInnitial}>{item.initName}</Text>
                                        <Text style={{ color: 'brown' }}>{item.name}</Text>
                                    </View>
                                    <View style={{ flex: 0.25 }}>
                                        <Switch
                                            trackColor={screenStyle.switchTrackColor}
                                            thumbColor="darkcyan"
                                            style={screenStyle.switchTransform}
                                            value={item.activate==1}
                                        />
                                    </View>
                                    <Popover
                                        from={(
                                            <Pressable>
                                                <MaterialCommunityIcons
                                                    name="menu"
                                                    color="darkgoldenrod"
                                                    size={20}
                                                />
                                            </Pressable>
                                        )}>
                                        <View style={{ flex: 1}}>
                                            
                                            <Text 
                                                style={{ padding:8, margin:4, backgroundColor: 'teal', color: "white" }}
                                                onPress={() => this.getItem(item.id, 1)}>Edit</Text>
                                        </View>
                                        <View style={{ flex: 0.1 }}>
                                            
                                            <Text 
                                                style={{ padding:8, margin:4, backgroundColor: 'brown', color: "white" }} 
                                                onPress={() => this.getItem(item.id, 2)}
                                            >Delete</Text>
                                        </View>
                                    </Popover>
                                </View>
                            );
                        })
                        }
                    </View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => { this.setModalVisible(!modalVisible) }}
                    >
                        <View style={[modalStyle.centeredView, { flex: 1, backgroundColor: 'rgba(0,0,0,.30)' }]}>
                            <View style={modalStyle.modalView}>
                                <FormCategory
                                    item={item}
                                    handleChange={this.handleChange}
                                    handleChangeCheckBox={this.handleChangeCheckBox}
                                    crudAction={crudAction}
                                />
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: .5, margin: 5 }}>
                                        <Pressable
                                            style={[modalStyle.button, modalStyle.buttonClose]}
                                            onPress={() => { this.submit() }}
                                        >
                                            <Text style={{ textAlign: "center", color: "white" }}>Save</Text>
                                        </Pressable>
                                    </View>
                                    <View style={{ flex: .5, margin: 5 }}>
                                        <Pressable
                                            style={[modalStyle.button, modalStyle.buttonClose]}
                                            onPress={() => { this.setModalVisible(!modalVisible) }}
                                        >
                                            <Text style={{ textAlign: "center", color: "white" }}>Close</Text>
                                        </Pressable>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    <Box position="relative">
                        <Fab
                            bottom={85}
                            position="absolute"
                            size="sm"
                            colorScheme="cyan"
                            onPress={() => { this.create() }}
                            icon={
                                <MaterialCommunityIcons
                                    name="plus"
                                    color="white"
                                    size={20}
                                />
                            }
                        />
                    </Box>
                </ScrollView>
            </NativeBaseProvider>
        );
    }
}
import AsyncStorage from '@react-native-community/async-storage';
import React, {useState, useEffect} from 'react';
import {ActivityIndicator, View, StyleSheet} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage'

const SplashScreen = ({ navigation })=>{
    const [animating, setAnimating] = useState(true);

    useEffect(()=>{
        setTimeout(()=>{
            AsyncStorage.getItem('user_token')
                .then((value)=>{
                    console.log(value);
                    navigation.replace(
                        value === null ? 'LoginScreen' : 'MainApp'
                    )
            })
        })
    })
    return (
        <View style={styles.container}>
            <ActivityIndicator
                animating= {animating}
                color='indigo'
                size='large'
                style={styles.activityIndicator}
            />
        </View>
    )
}
export default SplashScreen;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'indigo'
    },
    activityIndicator:{
        alignItems: 'center',
        height: 80
    }
})
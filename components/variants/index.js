import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Switch } from 'react-native';

import { screenStyle } from '../../styles/commonStyle';
import { CategoryService } from '../../services/variantServices';

export default class Variants extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
        };
    }

    componentDidMount() {
        this.load();
    }

    load = async () => {
        const response = await CategoryService.all();
        if (response.success) {
            this.setState({
                list: response.result,
            });
        } else {
            alert('Error: ' + response.result);
        }
    };

    render() {
        const { list } = this.state;
        return (
            <ScrollView style={screenStyle.container}>
                <View style={screenStyle.view}>
                    <Text style={screenStyle.title}>Welcome to Variants</Text>

                    {list.map((item, idx) => {
                        return (
                            <View key={idx} style={screenStyle.viewDetail}>
                                <View style={{ flex: 0.85 }}>
                                    <Text style={screenStyle.itemInnitial}>{item.catName}</Text>
                                    <Text style={{ color: 'gray' }}>{item.name}</Text>
                                </View>
                                <View style={{ flex: 0.15 }}>
                                    <Switch
                                        trackColor={screenStyle.switchTrackColor}
                                        thumbColor="darkcyan"
                                        style={screenStyle.switchTransform}
                                        value={Boolean(item.activate)}
                                    />
                                </View>
                            </View>
                        );
                    })}
                </View>
            </ScrollView>
        );
    }
}